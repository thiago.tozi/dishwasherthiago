"""
Here some global variables are declared which contain the current state and history of the PLC and measured values
"""

# object where the current washing state is kept
current = {
    'interval': 0,
    'name': '',
}

# object where all PLC i/os are kept
CURRENT_PLC_STATE = {}

# stores measured stack voltage for linear regression
voltage_history = []

# stores brine level switch status for delayed action on level control
brine_levels = []


# function clearing the voltage history and brine levels when required
def reset():
    del voltage_history[:]
    del brine_levels[:]
